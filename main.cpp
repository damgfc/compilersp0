/**************************/
/*        Project 0       */ 
/*         main.cpp       */
/**************************/
#include <iostream>
#include <ctime>
#include <cctype>
#include <string>
#include <fstream>
#include "buildTree.h"
#include "traversals.h"

using namespace std;
int main(int argc, char *argv[]){
	string incomingWord; 
	char keyLetter;
	string filename;
	//Handle arguments
	
	//They gave a file name
	if (argc == 2){
		filename = argv[1];
		filename.append(".dat");
		ifstream dataFile (filename.c_str());
		if(dataFile.is_open()){
			while(dataFile >> incomingWord){
				for (int i = 0; i < incomingWord.size(); i++){
					if (!isalpha(incomingWord.at(i))){
						cout << "Progam exiting - Error with invalid character: " << incomingWord.at(i) << endl;
						return 1;
					}
					else {
						if (i == 0)
						{	
							incomingWord.at(i) = toupper(incomingWord.at(i));
						}
						else {
							incomingWord.at(i) = tolower(incomingWord.at(i));
						}
					}
				}
				keyLetter = toupper(incomingWord.at(0));
				addLeaf(keyLetter, incomingWord);
			}
			dataFile.close();
		}
		else {
			cout << "Exiting due to input file not found\n";
			cout << "Please use files ending in \".dat\"\n";
			cout << "tree [filename]\n";
			return 1;
		}
	}

	// Only executable given
	else if (argc == 1) {
		cout << "Enter Some Words [press ctrl+d when finished]: ";
		while (cin >> incomingWord){
			for (int i = 0; i < incomingWord.size(); ++i){
				if (!isalpha(incomingWord.at(i))){
					cout << "Progam exiting - Error with invalid character: " << incomingWord.at(i) << endl;
					return 1;
				}
				else {
					if (i == 0)	{	
						incomingWord.at(i) = toupper(incomingWord.at(i));
					}
					else {
						incomingWord.at(i) = tolower(incomingWord.at(i));
					}
				}
			}
			keyLetter = incomingWord.at(0);
			addLeaf(keyLetter, incomingWord);
		}
	}

	//Too many arguments
	else {
		cout << "Your input is not valid. Please try the following format:\n";
		cout << "tree [filename]\n";
		return 0;
	}

	// Display output
	node *root = getRoot();
	cout << "inOrder\n-----------------\n";
	inOrder(root);
	cout << "postOrder\n-----------------\n";
	postOrder(root);
	cout << "preOrder\n-----------------\n";
	preOrder(root);
	cout << endl << endl;

	return 0;
}

