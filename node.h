/**************************/
/*        Project 0       */ 
/*         node.h         */
/**************************/
#ifndef NODE_H
#define NODE_H
#include <vector>
#include <string>

using namespace std;

struct node {
	char keyLetter;
	vector<string> wordsInNodeVector;
	node *left;
	node *middle;
	node *right;
	node *parent;
	int level;
};

#endif
