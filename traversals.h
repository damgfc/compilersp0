/**************************/
/*     traversals.h       */
/**************************/
#include "node.h"
void inOrder(node *n);
void preOrder(node *n);
void postOrder(node *n);
